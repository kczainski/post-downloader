package com.kczainski.postdownloader.persistence.files;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kczainski.postdownloader.beans.Post;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.nio.file.Files;

@Service
@Slf4j
public class FileService {

	@Value("${persistence.file.post.directory}")
	private String postDirectoryPrefix;

	private ObjectMapper objectMapper = new ObjectMapper();

	/**
	 * Saves post within directory: ./posts/user_{userId}/{postId}_{postTitle}.json
	 *
	 * @param post Post to save
	 */
	public void savePost(Post post) {
		try {
			saveFile(prepareFileNameDirectory(post), objectMapper.writeValueAsString(post));
		} catch (JsonProcessingException e) {
			log.error("Failed to map Post into json, post:{}", post, e);
		}
	}

	/**
	 * Saves content into specified file
	 *
	 * @param fileName fileName with directory (for example "/documents/doc.txt")
	 */
	private void saveFile(String fileName, String content) {
		try {
			log.debug("Writing to:{}", fileName);
			String rootPath = new File(".").getCanonicalPath();
			fileName = rootPath + fileName;
			File file = new File(fileName);
			Files.createDirectories(file.getParentFile().toPath());
			Writer writer = new BufferedWriter(new FileWriter(file));
			writer.write(content);
			writer.close();

		} catch (Exception e) {
			log.error("Error writing to file:{}", fileName, e);
		}
	}

	/**
	 * Builds file name with directory. Directory is separated for each user(id).
	 * File naming is {ID}_{TITLE}
	 *
	 * @param post Post to build file path
	 * @return String file name with directory
	 */
	private String prepareFileNameDirectory(Post post) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(postDirectoryPrefix);
		stringBuilder.append("/");
		stringBuilder.append("user_");
		stringBuilder.append(post.getUserId());
		stringBuilder.append("/");
		stringBuilder.append(post.getId());
		stringBuilder.append("_");
		stringBuilder.append(post.getTitle());
		stringBuilder.append(".json");
		return stringBuilder.toString();
	}
}
