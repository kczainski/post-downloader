package com.kczainski.postdownloader.businesslogic;

import com.kczainski.postdownloader.beans.Post;
import com.kczainski.postdownloader.persistence.files.FileService;
import com.kczainski.postdownloader.web.service.PostsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class PostsManager {

	@Autowired
	@Qualifier("rest")
	private PostsService postsService;

	@Autowired
	private FileService fileService;

	public int downloadPosts() {
		List<Post> posts;
		Optional<List<Post>> optionalPosts = postsService.getPosts();
		if (optionalPosts.isPresent()) {
			posts = optionalPosts.get();
			log.debug("Retrieved {} posts", posts.size());
			posts.forEach(post -> fileService.savePost(post));
			return posts.size();
		} else {
			log.warn("Failed to retrieve posts!");
			return 0;
		}
	}


}
