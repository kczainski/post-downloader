package com.kczainski.postdownloader.web.controllers.rest;

import com.kczainski.postdownloader.businesslogic.PostsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/posts")
public class PostsRestController {

	@Autowired
	private PostsManager postsManager;

	@PostMapping("/download")
	public ResponseEntity<Integer> downloadPosts() {
		int postsDownloaded = postsManager.downloadPosts();
		if (postsDownloaded > 0) {
			return ResponseEntity.ok(postsDownloaded);
		} else {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(0);
		}
	}

}
