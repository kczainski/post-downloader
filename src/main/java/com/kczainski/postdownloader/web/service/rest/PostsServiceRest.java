package com.kczainski.postdownloader.web.service.rest;

import com.kczainski.postdownloader.beans.Post;
import com.kczainski.postdownloader.web.service.PostsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service("rest")
@Slf4j
public class PostsServiceRest implements PostsService {

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public Optional<List<Post>> getPosts() {
		ResponseEntity<Post[]> responseEntity = restTemplate.getForEntity(
				"https://jsonplaceholder.typicode.com/posts", Post[].class);
		log.debug("getPosts returned status Code:{}", responseEntity.getStatusCode());
		if (responseEntity.getStatusCode().is2xxSuccessful() && responseEntity.getBody() != null) {
			log.debug("getPosts returned {} posts", responseEntity.getBody().length);
			if (responseEntity.getBody() != null) {
				return Optional.of(Arrays.asList(responseEntity.getBody()));
			}
		}
		return Optional.empty();
	}

}
