package com.kczainski.postdownloader.web.service;

import com.kczainski.postdownloader.beans.Post;

import java.util.List;
import java.util.Optional;

public interface PostsService {

	Optional<List<Post>> getPosts();
}
