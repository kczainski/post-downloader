package com.kczainski.postdownloader.web.service.rest;

import com.kczainski.postdownloader.beans.Post;
import com.kczainski.postdownloader.web.service.PostsService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class PostServiceRestTest {

	@Autowired
	@Qualifier("rest")
	private PostsService postsService;

	@DisplayName("Test retrieving first post")
	@Test
	void test() {
		List<Post> posts = postsService.getPosts().get();
		Assertions.assertNotNull(posts.get(0));
	}
}
