package com.kczainski.postdownloader.businesslogic;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.constraints.NotNull;
import java.io.File;

@SpringBootTest
public class PostsManagerTest {

	@Autowired
	private PostsManager postsManager;

	@Value("${persistence.file.post.directory}")
	private String postDirectoryPrefix;

	@DisplayName("Test downloading posts")
	@Test
	void downloadPostsTest() {
		int downloadedPosts = postsManager.downloadPosts();
		File postsFile = new File("." + postDirectoryPrefix);
		Assertions.assertEquals(downloadedPosts, countAndDeleteFiles(0, postsFile));
	}

	@Disabled
	@DisplayName("Test downloading posts")
	@Test
	void downloadAndSavePostsTest() {
		int downloadedPosts = postsManager.downloadPosts();
		File postsFile = new File("." + postDirectoryPrefix);
		Assertions.assertEquals(downloadedPosts, countFiles(0, postsFile));
	}

	private int countFiles(int count, @NotNull File file) {
		if (file.list() != null) {
			for (File childFile : file.listFiles()) {
				count = countFiles(count, childFile);
			}
			return count;
		} else {
			return ++count;
		}
	}

	private int countAndDeleteFiles(int count, @NotNull File file) {
		if (file.list() != null) {
			for (File childFile : file.listFiles()) {
				count = countAndDeleteFiles(count, childFile);
			}
			file.delete();
			return count;
		} else {
			file.delete();
			return ++count;
		}
	}

}
