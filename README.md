# Post downloader

Basic application for downloading posts from https://jsonplaceholder.typicode.com. Application groups downloaded posts by users. For example Post authored by user with ID:1, postId:10 and postTitle:sampleTitle will go to:
`./posts/user_1/10_sampleTitle.json`

### Technologies
Application is based on spring boot, and allows to download all posts in JSON format from https://jsonplaceholder.typicode.com. Application preforms download after receiving empty POST request on /posts/download.

### Running  
Application uses lombok so in order to run You have to have lombok installed. Application uses gradle for building.
If You want to get files without sending Request, You can enable downloadAndSavePostsTest Method in PostsManagerTest.class as it is disabled since it leaves files on disc. (There is other test method in use that deletes files after download)